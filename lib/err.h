#pragma once

#include <stdint.h>

namespace err {

__attribute__((noreturn))
void die(uint16_t code, const char* msg);
void set(uint16_t code, const char* msg);
uint16_t get();

class fatal {
	bool old_fatal;
public:
	fatal();
	~fatal();
};

}
