#pragma once

#include <stdint.h>

namespace dos {
namespace fs {

enum class Mode: byte {
	Read = 0x01,
	Write = 0x02,
	Append = 0x04
};

typedef WORD FILE_HANDLE;
const FILE_HANDLE INVALID_FILE = 0xFFFF;

class File {
	FILE_HANDLE fh;
	
	inline File(): fh(INVALID_FILE) {}
	inline explicit File(FILE_HANDLE fh): fh(fh) {}
public:
	static File Open(const char* filename, Mode mode);
	
	void write(uint16_t len, const byte* data);
	
	~File();
	
	constexpr File(File &&) = default;
	File(File const &) = delete;
	void operator=(File const &) = delete;
};

FILE_HANDLE create_truncate(const char* filename);
uint16_t write(FILE_HANDLE fh, uint16_t len, const byte* data);
void close(FILE_HANDLE fh);

}
}
