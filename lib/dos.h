#pragma once

#include <stdint.h>

namespace dos {

uint8_t get_commandline(const char*& cline);

void print(const char* s);

uint16_t version();

__attribute__((noreturn))
void exit(uint8_t code);

}
