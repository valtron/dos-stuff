#include "dos.h"

#include <stdint.h>

namespace dos {

uint8_t get_commandline(const char*& cline) {
	cline = (const char*)0x81;
	return *((uint8_t*)0x80);
}

void print(const char* s) {
	asm volatile (
		"mov $0x09, %%ah \n\t"
		"int $0x21       \n\t"
		: //
		: "d"(s)
		: "a"
	);
}

uint16_t version() {
	uint16_t v;
	asm (
		"int $0x21 \n\t"
		: "=a"(v)
		: "a"(0x3000)
		: "b", "c"
	);
	if ((v & 0xFF) == 0x00) v = 0x0001;
	return v << 8 | v >> 8;
}

__attribute__((noreturn))
void exit(uint8_t code) {
	asm volatile (
		"jmp __com__end \n\t"
		: : "a"(code)
	);
	__builtin_unreachable();
}

}
