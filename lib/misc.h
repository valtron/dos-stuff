#define RESERVE_REGISTER(name) _RESERVE_REGISTER_1(name, __COUNTER__)
#define _RESERVE_REGISTER_1(name, count) register int _RESERVE_REGISTER_2(name, count) asm(#name);
#define _RESERVE_REGISTER_2(name, count) _do_not_use_ ## name ## _ ## count

#define WITH(context_manager) _WITH_1(context_manager, __COUNTER__)
#define _WITH_1(context_manager, count) context_manager _WITH_2(count)
#define _WITH_2(count) _context_class_ ## count

#define CONTEXT_MANAGER(name, ctor, dtor) \
	struct name { \
		name() ctor \
		~name() dtor \
	}

#define HL(h, l) ((((uint16_t)(h))<<8) | ((uint8_t)(l)))
