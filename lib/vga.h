#pragma once

#include <stdint.h>
#include <bios.h>
#include <misc.h>

RESERVE_REGISTER(es);

const int16_t SCREEN_WIDTH = 320;
const int16_t SCREEN_HEIGHT = 200;
const uint16_t VGA_MEM_BASE = 0xA000;

namespace {
	const uint16_t PORT_VSYNC = 0x03DA;
}

class VGA {
public:
	VGA() {
		bios::set_video_mode(bios::VideoMode::Graphics);
		asm volatile (
			"mov %0, %%es \n\t"
			: // out
			: "r"(VGA_MEM_BASE)
			: //
		);
	}
	~VGA() {
		bios::set_video_mode(bios::VideoMode::Text);
		asm volatile (
			"mov %0, %%es \n\t"
			: // out
			: "r"(VGA_MEM_BASE)
			: //
		);
	}
	
	void set_base(uint16_t addr) {
		asm volatile (
			"out %%ax, %%dx \n\t"
			: // out
			: "a"(0x0C | (addr & 0xFF00)), "d"(0x03D4)
			: //
		);
		
		asm volatile (
			"out %%ax, %%dx \n\t"
			: // out
			: "a"(0x0D | (((uint16_t)addr) << 8)), "d"(0x03D4)
			: //
		);
	}
	
	void put_pixel(int16_t x, int16_t y, int8_t color) {
		if (x < SCREEN_WIDTH && y < SCREEN_HEIGHT) {
			int16_t offset = y * SCREEN_WIDTH + x;
			asm volatile (
				"movb %%cl, %%es:(%%bx) \n\t"
				: // out
				// TODO: check if still: "has to be cx (not cl) because of optimizer"
				: "b"(offset), "c"(color)
				: //
			);
		}
	}
	
	void clear(uint8_t c) {
		WORD a = (((uint16_t)c) << 8) | c;
		uint16_t ignored;
		// GCC doesn't seem to actually support %di ("D")
		asm volatile (
			"push %%di    \n\t"
			"mov $0, %%di \n\t"
			"cld          \n\t"
			"rep stosw    \n\t"
			"pop %%di     \n\t"
			: "=c"(ignored)
			: "a"(a), "c"(SCREEN_WIDTH * (SCREEN_HEIGHT / sizeof(WORD)))
			: "memory"
		);
	}
	
	void clear_row(int16_t y, uint8_t c) {
		WORD a = (((uint16_t)c) << 8) | c;
		uint16_t ignored;
		// GCC doesn't seem to actually support %di ("D")
		asm volatile (
			"push %%di      \n\t"
			"mov %%dx, %%di \n\t"
			"cld            \n\t"
			"rep stosw      \n\t"
			"pop %%di       \n\t"
			: "=c"(ignored)
			: "a"(a), "d"(y * 320), "c"(SCREEN_WIDTH / sizeof(WORD))
			: "memory"
		);
	}
	
	void put_pixel_unsafe(int16_t x, int16_t y, uint8_t color) {
		uint16_t offset = y * SCREEN_WIDTH + x;
		asm volatile (
			"movb %%cl, %%es:(%%bx) \n\t"
			: // out
			: "b"(offset), "c"(color)
			: "memory"
		);
	}
	
	void wait_for_not_retrace() {
		asm volatile (
			"\ntop%=: \n\t"
			"in  %%dx, %%al \n\t"
			"and $0x8, %%al \n\t"
			"jnz top%=      \n\t"
			: //
			: "d"(PORT_VSYNC)
			: "a"
		);
		// At this point, it's in display mode.
		// ~199/200 of total frame time is spent here
	}
	
	void wait_for_retrace() {
		asm volatile (
			"\ntop%=: \n\t"
			"in  %%dx, %%al \n\t"
			"and $0x8, %%al \n\t"
			"jz  top%=      \n\t"
			: //
			: "d"(PORT_VSYNC)
			: "a"
		);
		// At this point, it's in vertical retrace mode.
		// ~1/200 of total frame time is spent here
	}
	
	void vsync() {
		wait_for_not_retrace();
		wait_for_retrace();
	}
};
