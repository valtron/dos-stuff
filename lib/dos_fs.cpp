#include "dos_fs.h"

#include <stdint.h>
#include <err.h>
#include <misc.h>

namespace dos {
namespace fs {

namespace {
	const FILE_HANDLE STDIN  = 0x0000;
	const FILE_HANDLE STDOUT = 0x0001;
	const FILE_HANDLE STDERR = 0x0002;
}

// Low-level functions

FILE_HANDLE create_truncate(const char* filename) {
	/*
		int 0x21 ah(0x3C)
			cx    file_attribs
			ds:dx filename_asciz
		->
		cf
			0 success
				ax = handle
			1 error
				ax = error (0x03, 0x04, 0x05)
	*/
	uint16_t ax;
	WORD flags;
	asm volatile (
		"pushf     \n\t"
		"int $0x21 \n\t"
		"pop %%cx  \n\t"
		: "=a"(ax), "=c"(flags)
		: "a"(HL(0x3C, 0x00)), "c"(0x0), "d"(filename)
		: "cc"
	);
	
	if (flags & 1) {
		err::set(ax, "dos::fs::create_truncate");
		return INVALID_FILE;
	}
	
	return ax;
}

uint16_t write(FILE_HANDLE fh, uint16_t len, const byte* data) {
	/*
		int 0x21 ah(0x40)
			bx    handle
			cx    data length
			ds:dx data
		->
		cf
			0 success
				ax = bytes written
			1 error
				ax = error (0x05, 0x06)
	*/
	
	uint16_t ax;
	WORD flags;
	asm volatile (
		"pushf     \n\t"
		"int $0x21 \n\t"
		"pop %%cx  \n\t"
		: "=a"(ax), "=c"(flags)
		: "a"(HL(0x40, 0x00)), "b"(fh), "c"(len), "d"(data)
		: "cc"
	);
	
	if (flags & 1) {
		err::set(ax, "dos::fs::write");
		return 0;
	}
	
	return ax;
}

void close(FILE_HANDLE fh) {
	/*
		int 0x21 ah(0x3E)
			bx handle
		->
		cf
			0 success
			1 error
				ax = error (0x06)
	*/
	
	uint16_t ax;
	WORD flags;
	asm volatile (
		"pushf     \n\t"
		"int $0x21 \n\t"
		"pop %%cx  \n\t"
		: "=a"(ax), "=c"(flags)
		: "a"(HL(0x3E, 0x00)), "b"(fh)
		: "cc"
	);
	
	if (flags & 1) {
		err::set(ax, "dos::fs::close");
	}
}

File File::Open(const char* filename, Mode mode) {
	FILE_HANDLE fh;
	if (mode == Mode::Write) {
		fh = create_truncate(filename);
	} else {
		// TODO: Handle other modes
		err::set(0x0100, "File::Open: given `mode` not implemented");
		fh = INVALID_FILE;
	}
	return File(fh);
}

void File::write(uint16_t len, const byte* data) {
	::dos::fs::write(fh, len, data);
}

File::~File() {
	if (fh != INVALID_FILE) close(fh);
}

}
}
