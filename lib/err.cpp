#include "err.h"

#include <stdint.h>
#include <dos.h>
#include <debug.h>

namespace err {

namespace {
	bool FATAL = true;
	bool LOG = false;
	uint16_t ERRNO = 0;
	
	void log(uint16_t code, const char* msg) {
		debug::print("Error ");
		debug::print(code);
		debug::print(": ");
		debug::print(msg);
		debug::print("\n");
	}
}

fatal::fatal(): old_fatal(FATAL) {}
fatal::~fatal() { FATAL = old_fatal; }

__attribute__((noreturn))
void die(uint16_t code, const char* msg) {
	log(code, msg);
	dos::exit(0xFF);
	__builtin_unreachable();
}

void set(uint16_t code, const char* msg) {
	if (FATAL) die(code, msg);
	if (LOG) log(code, msg);
	ERRNO = code;
}

uint16_t get() {
	uint16_t tmp = ERRNO;
	ERRNO = 0;
	return tmp;
}

}
