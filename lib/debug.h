#pragma once

#include <stdint.h>
#include <dos_fs.h>

namespace debug {

inline uint16_t strlen(const char* s) {
	const char* s0 = s;
	while (*s0) s0++;
	return s0 - s;
}

inline void print(const char* s) {
	uint16_t l = strlen(s);
	dos::fs::write(2, l, (const byte*)s);
}

inline void print(uint16_t n) {
	volatile char buffer[6];
	int i = sizeof(buffer);
	buffer[--i] = '\0';
	if (n == 0) {
		buffer[--i] = '0';
	} else {
		for (; n > 0; n /= 10)
			buffer[--i] = '0' + (n % 10);
	}
	print((const char *)buffer + i);
}

}
