#pragma once

#include "bios.h"
#include "debug.h"

const uint8_t KEY_ESC   = 0x01;
const uint8_t KEY_UP    = 0x48;
const uint8_t KEY_DOWN  = 0x50;
const uint8_t KEY_LEFT  = 0x4B;
const uint8_t KEY_RIGHT = 0x4D;
const uint8_t KEY_SPACE = 0x39;
const uint8_t KEY_Z = 0x2C;
const uint8_t KEY_X = 0x2D;

const uint8_t KEY_MAX = 0x80;
volatile bool KEY_MAP[KEY_MAX];

BIOS_INTERRUPT_SERVICE_ROUTINE(0x09, keyboard_isr) {
	uint8_t scan;
	asm volatile (
		"in $0x60, %%al \n\t"
		: "=ax"(scan)
		: // in
		: //
	);
	
	uint8_t k = scan & 0x7F;
	if (scan & 0x80) {
		KEY_MAP[k] = false;
	} else {
		KEY_MAP[k] = true;
	}
}

class Keyboard {
	WITH(keyboard_isr);
public:
	Keyboard() {
		for (int i = 0; i < KEY_MAX; ++i) {
			KEY_MAP[i] = false;
		}
	}
};
