#pragma once

#include <stdint.h>
#include <misc.h>

namespace bios {

enum class VideoMode: uint8_t {
	// VGA Modes
	Text = 0x03, // 80x25 @B800
	Graphic_320x200x16 = 0x0d,
	Graphics = 0x13 // 320x200 @A000
};

typedef void (*interrupt_t)();

struct segptr {
	// offset field has to be first, e.g. for `ljmpw *old_handler`
	uint16_t offset;
	uint16_t segment;
	
	segptr(): offset(0), segment(0) {}
	segptr(interrupt_t f): offset((uint16_t)(int)f) {
		asm volatile (
			"push %%cs \n\t"
			"pop  %%ax \n\t"
			: "=a"(this->segment)
		);
	}
};

// Should "cld" be added?
#define BIOS_INTERRUPT_SERVICE_ROUTINE(interrupt, name) \
	extern "C" void _bios_isrb_ ## name(); \
	extern "C" void _bios_isrl_ ## name(); \
	asm volatile ( \
		"\n_bios_isrl_" #name ": \n\t" \
		"pushaw \n\t" \
		"call _bios_isrb_" #name " \n\t" \
		"popaw \n\t" \
		"ljmpw *_bios_isro_" #name " \n\t" \
	); \
	__attribute__((externally_visible)) \
	::bios::segptr _bios_isro_ ## name; \
	CONTEXT_MANAGER(name, \
		{ ::bios::push_interrupt_handler(interrupt, _bios_isrl_ ## name, _bios_isro_ ## name); }, \
		{ ::bios::pop_interrupt_handler(interrupt, _bios_isro_ ## name); } \
	); \
	extern "C" __attribute__((externally_visible)) void _bios_isrb_ ## name()

void set_video_mode(VideoMode mode) {
	asm volatile (
		"int $0x10 \n\t"
		: //
		: "a"(HL(0x00, mode))
		: //
	);
}

void save_interrupt_handler(uint8_t interrupt, segptr& out_old_handler) {
	// Need to push/pop es, because adding it to clobber
	// gives "error: unknown register name '%ds' in 'asm'"
	asm volatile (
		"push %%es      \n\t"
		"int $0x21      \n\t"
		"mov %%es, %%ax \n\t"
		"pop %%es       \n\t"
		: "=b"(out_old_handler.offset), "=a"(out_old_handler.segment)
		: "a"(HL(0x35, interrupt))
		: //
	);
}

void set_interrupt_handler(uint8_t interrupt, segptr ptr) {
	// Need to push/pop ds, because ... (same as above)
	asm volatile (
		"push %%ds    \n\t"
		"mov %2, %%ds \n\t"
		"int $0x21    \n\t"
		"pop %%ds     \n\t"
		: // out
		: "a"(HL(0x25, interrupt)), "d"(ptr.offset), "r"(ptr.segment)
		: //
	);
}

CONTEXT_MANAGER(cli,
	{ asm volatile ("cli"); },
	{ asm volatile ("sti"); }
);

void push_interrupt_handler(uint8_t interrupt, interrupt_t handler, segptr& out_old_handler) {
	WITH(cli);
	save_interrupt_handler(interrupt, out_old_handler);
	set_interrupt_handler(interrupt, segptr { handler });
}

void pop_interrupt_handler(uint8_t interrupt, segptr& old_handler) {
	WITH(cli);
	set_interrupt_handler(interrupt, old_handler);
}

}
