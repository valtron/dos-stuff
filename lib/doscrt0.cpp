#include "stdint.h"

asm (
	".section .comstartup \n\t"
	".global __com__start \n"
	"__com__start:        \n\t"
	"call __run_ctors     \n\t"
	"call main            \n\t"
	".global __com__end   \n"
	"__com__end:          \n\t"
	"push %ax             \n\t"
	"call __run_dtors     \n\t"
	"pop  %ax             \n\t"
	"mov  $0x4C, %ah      \n\t"
	"int  $0x21           \n\t"
);

typedef void (*func_t)();

extern func_t __CTOR_LIST__[];
extern func_t __DTOR_LIST__[];

extern "C"
__attribute__((externally_visible))
void __run_ctors() {
	for (int i = 0; ; ++i) {
		func_t f = __CTOR_LIST__[i];
		if (!f) break;
		f();
	}
}

extern "C"
__attribute__((externally_visible))
void __run_dtors() {
	for (int i = 0; ; ++i) {
		func_t f = __DTOR_LIST__[i];
		if (!f) break;
		f();
	}
}
