#pragma once

// Numerics
typedef unsigned char     uint8_t;
typedef char              int8_t;
typedef unsigned short    uint16_t;
typedef short             int16_t;
typedef unsigned long int uint32_t;
typedef long int          int32_t;

// Other
typedef unsigned char byte;
typedef uint16_t WORD;
typedef uint32_t DWORD;
