#include <debug.h>
#include <dos.h>

struct S {
	S() {
		debug::print("ctor\n");
	}
	~S() {
		debug::print("dtor\n");
	}
} global_s;

uint8_t main() {
	debug::print("char: ");
	debug::printl(sizeof(char));
	debug::print("\n");
	
	debug::print("short: ");
	debug::printl(sizeof(short));
	debug::print("\n");
	
	debug::print("int: ");
	debug::printl(sizeof(int));
	debug::print("\n");
	
	debug::print("int*: ");
	debug::printl(sizeof(int*));
	debug::print("\n");
	
	debug::print("long: ");
	debug::printl(sizeof(long));
	debug::print("\n");
	
	debug::print("long long: ");
	debug::printl(sizeof(long long));
	debug::print("\n");
	
	uint16_t v = dos::version();
	uint8_t vh = v >> 8;
	uint8_t vl = v & 0xFF;
	debug::print("DOS ");
	debug::printl(vh);
	debug::print(".");
	debug::printl(vl);
	debug::print("\n");
	
	return 0;
}
