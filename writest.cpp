#include <dos.h>
#include <dos_fs.h>
#include <stdint.h>
#include <misc.h>

uint8_t main() {
	const char* fn = "C:\\new.txt";
	
	const char* cline;
	uint8_t clen = dos::get_commandline(cline);
	
	dos::fs::File file = dos::fs::File::Open(fn, dos::fs::Mode::Write);
	file.write(clen, (const byte*)cline);
	
	return 0;
}
