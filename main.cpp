#include <stdint.h>
#include <debug.h>
#include <vga.h>
#include <bios.h>
#include <dos.h>
#include <keyboard.h>
#include <misc.h>

void draw_rect(VGA& vga, int16_t x, int16_t y, uint8_t c) {
	int16_t r = x + 50;
	int16_t b = y + 100;
	
	if (x < 0) x = 0;
	if (y < 0) y = 0;
	if (r > SCREEN_WIDTH) r = SCREEN_WIDTH;
	if (b > SCREEN_HEIGHT) b = SCREEN_HEIGHT;
	
	for (; x < r; ++x) {
		for (int16_t i = y; i < b; ++i) {
			vga.put_pixel_unsafe(x, i, c);
		}
	}
}

void test_halt_loop() {
	while (!KEY_MAP[KEY_SPACE]) asm volatile ("hlt");
}

struct State {
	int16_t x;
	int16_t y;
};

void test_time() {
	while (!KEY_MAP[KEY_ESC]) {
		uint16_t cx;
		uint16_t dx;
		uint8_t al;
		asm volatile (
			"int $0x1A \n\t"
			: "=c"(cx), "=d"(dx), "=a"(al)
			: "a"(HL(0x00, 0x00))
			: //
		);
		debug::print(cx);
		debug::print(" ");
		debug::print(dx);
		debug::print("\n");
	}
}

uint16_t get_time_cx() {
	uint16_t cx;
	uint16_t dx;
	uint8_t al;
	asm volatile (
		"int $0x1A \n\t"
		: "=c"(cx), "=d"(dx), "=a"(al)
		: "a"(HL(0x00, 0x00))
		: //
	);
	return dx;
}

void time_code() {
	uint16_t ticks_vsync = 0;
	uint16_t ticks_rect = 0;
	uint16_t ticks_clear = 0;
	uint16_t ticks_row = 0;
	
	{
		VGA vga;
		
		uint16_t a, b;
		
		a = get_time_cx();
		for (int i = 0; i < 200; ++i) {
			vga.vsync();
		}
		b = get_time_cx();
		ticks_vsync = (b - a);
		
		a = get_time_cx();
		for (int i = 0; i < 30; ++i) {
			draw_rect(vga, 10, 11, 13);
		}
		b = get_time_cx();
		ticks_rect = (b - a);
		
		a = get_time_cx();
		for (uint16_t i = 0; i < 100; ++i) {
			vga.clear(0);
		} 
		b = get_time_cx();
		ticks_clear = (b - a);
		
		a = get_time_cx();
		for (uint16_t i = 0; i < 50000; ++i) {
			vga.clear_row(i & 0x7F, 0);
		} 
		b = get_time_cx();
		ticks_row = (b - a);
	}
	
	/*
		At 3000 cycles:
		PHASE  TICKS   REPS  TIME (ms)
		vsync    52     200   14.29 (= 70fps)
		rect      4      30   13.55
		clear    19     100   10.44
		row      54   50000    0.05934
	*/
	
	debug::print(ticks_vsync);
	debug::print(" ");
	debug::print(ticks_rect);
	debug::print(" ");
	debug::print(ticks_clear);
	debug::print(" ");
	debug::print(ticks_row);
	debug::print("\n");
}

uint8_t Sprite[2][256] = {
	{
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
		7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
	},
	{
		0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
		0,  0, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,  0,  0,
		0,  3,  0, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,  0,  3,  0,
		0,  3,  3,  0, 11, 11, 11, 11, 11, 11, 11, 11,  0,  3,  3,  0,
		0,  3,  3,  3,  0, 11, 11, 11, 11, 11, 11,  0,  3,  3,  3,  0,
		0,  3,  3,  3,  3,  0, 11, 11, 11, 11,  0,  3,  3,  3,  3,  0,
		0,  3,  3,  3,  3,  3,  0, 11, 11,  0,  3,  3,  3,  3,  3,  0,
		0,  3,  3,  3,  3,  3,  3,  0,  0,  3,  3,  3,  3,  3,  3,  0,
		0,  3,  3,  3,  3,  3,  3,  0,  0,  3,  3,  3,  3,  3,  3,  0,
		0,  3,  3,  3,  3,  3,  0,  1,  1,  0,  3,  3,  3,  3,  3,  0,
		0,  3,  3,  3,  3,  0,  1,  1,  1,  1,  0,  3,  3,  3,  3,  0,
		0,  3,  3,  3,  0,  1,  1,  1,  1,  1,  1,  0,  3,  3,  3,  0,
		0,  3,  3,  0,  1,  1,  1,  1,  1,  1,  1,  1,  0,  3,  3,  0,
		0,  3,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  0,  3,  0,
		0,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  0,  0,
		0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	}
};

uint8_t World[5][5] = {
	{ 0, 0, 0, 0, 0 },
	{ 1, 1, 1, 1, 0 },
	{ 1, 0, 0, 1, 0 },
	{ 1, 0, 1, 1, 0 },
	{ 1, 1, 1, 1, 0 },
};

void draw_section(State& s, VGA& vga, int16_t l, int16_t t, int16_t w, int16_t h) {
	int16_t r = l + w;
	int16_t b = t + h;
	
	int16_t Tl = (s.x+l) / 16;
	int16_t Tr = (s.x+r) / 16;
	uint8_t subtile_l = (s.x+l) & 0x0F;
	
	for (int16_t y = t; y < b; ++y) {
		uint16_t wy = y + s.y;
		uint16_t tile_y = (wy / 16) % 5;
		uint8_t subtile_y = wy & 0x0F;
		
		uint16_t wx = s.x+l;
		
		for (int16_t T = Tl; T <= Tr; ++T) {
			uint16_t tile_x = (wx / 16) % 5;
			uint16_t tile_w = 16;
			uint8_t sprite = World[tile_y][tile_x];
			uint8_t* p = &Sprite[sprite][subtile_y * 16];
			
			if (T == Tl) {
				tile_w -= subtile_l;
				p += subtile_l;
			}
			if (T == Tr) {
				tile_w = r - wx + s.x;
			}
			uint16_t offset = wx + wy * 320;
			
			uint16_t garbage;
			asm volatile (
				"rep movsb \n\t" // ds:si -> es:di
				: "=c"(garbage)
				: "c"(tile_w), "S"(p), "D"(offset)
				: //
			);
			
			wx += 16;
			if (T == Tl) {
				wx -= subtile_l;
			}
		}
	}
}

void out_port(WORD port, byte lo) {
	asm volatile ("out %%al, %%dx \n\t": : "a"(lo), "d"(port));
}

void out_port(WORD port, byte lo, byte hi) {
	asm volatile ("out %%ax, %%dx \n\t": : "a"(HL(hi, lo)), "d"(port));
}

byte in_port(WORD port) {
	byte ret;
	asm volatile ("in %%dx, %%al \n\t": "=a"(ret) : "d"(port));
	return ret;
}

uint8_t main() {
	//time_code();
	
	Keyboard kbd;
	
	State s { 0, 0 };
	
	VGA vga;
	
	draw_section(s, vga, 0, 0, 320, 200);
	
	while (!KEY_MAP[KEY_ESC]) {
		vga.wait_for_not_retrace();
		
		uint16_t addr = s.x + s.y * 320;
		vga.set_base(addr/4);
		out_port(0x03C0, 0x13);
		out_port(0x03C0, (addr & 0x03) << 1);
		
		vga.wait_for_retrace();
		
		if (KEY_MAP[KEY_UP]) {
			s.y -= 1;
			draw_section(s, vga, 0, 0, 320, 1);
		} else if (KEY_MAP[KEY_DOWN]) {
			s.y += 1;
			draw_section(s, vga, 0, 200-1, 320, 1);
		} else if (KEY_MAP[KEY_LEFT]) {
			s.x -= 1;
			draw_section(s, vga, 0, 0, 1, 200);
		} else if (KEY_MAP[KEY_RIGHT]) {
			s.x += 1;
			draw_section(s, vga, 320-1, 0, 1, 200);
		}
		
		//vga.clear(2); // dark green
		//vga.clear(3); // dark cyan
		//vga.clear(4); // dark red
		//vga.clear(5); // dark pink
		//vga.clear(6); // dark yellow
		//draw_rect(vga, x, y, color);
	}
	
	return 0;
}
