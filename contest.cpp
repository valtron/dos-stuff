#include <stdint.h>
#include <dos.h>
#include <dos_fs.h>
#include <debug.h>
#include <misc.h>

uint8_t main() {
	const char* cline;
	uint8_t clen = dos::get_commandline(cline);
	
	dos::fs::write(1, clen, (const byte*)cline);
	dos::fs::write(2, clen, (const byte*)cline);
	
	return 0;
}
