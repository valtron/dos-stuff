ifeq (,$(wildcard config.mk))
$(error Please create config.mk -- see config.mk.sample.)
endif

include config.mk

CXXFLAGS = -std=c++17 -Wall -Wextra -Wno-unused-function -Os -nostdlib \
	-ffreestanding -fomit-frame-pointer -fwrapv -fno-strict-aliasing \
	-fno-use-cxa-atexit -fno-leading-underscore -fno-pic \
	-fdata-sections -ffunction-sections -fno-exceptions \
	-march=i80286
LDFLAGS = --nmagic -static -Tcom.ld -fuse-ld=bfd --gc-sections
ASFLAGS = -march=i386

LIB := lib
TMP := tmp
BIN := bin

COMS = $(patsubst %.cpp,%.com,$(wildcard *.cpp))
LIBS = $(patsubst $(LIB)/%.cpp,$(BIN)/%.o,$(wildcard $(LIB)/*.cpp))

all: $(COMS) $(LIBS)

.PHONY: all clean
.PRECIOUS: $(TMP)/%.s $(TMP)/%.o

clean:
	$(RM) -r *.com $(BIN) $(TMP)

sysinfo.com: $(BIN)/doscrt0.o $(BIN)/err.o $(BIN)/dos.o $(BIN)/dos_fs.o $(BIN)/other.o
contest.com: $(BIN)/doscrt0.o $(BIN)/err.o $(BIN)/dos.o $(BIN)/dos_fs.o
main.com   : $(BIN)/doscrt0.o $(BIN)/err.o $(BIN)/dos.o $(BIN)/dos_fs.o
writest.com: $(BIN)/doscrt0.o $(BIN)/err.o $(BIN)/dos.o $(BIN)/dos_fs.o

%.com: $(TMP)/%.o
	$(LD) $(LDFLAGS) -o $@ $^

$(BIN)/%.o: $(TMP)/%.o
	@mkdir -p $(BIN)
	cp $< $@

$(TMP)/%.o: $(TMP)/%.s
	@mkdir -p $(TMP)
	$(AS) $(ASFLAGS) -o $@ $<

$(TMP)/%.s: %.cpp $(LIB)/*.h
	@mkdir -p $(TMP)
	$(CXX) $(CXXFLAGS) -fwhole-program -I$(LIB) -S $< -o $@
	sed --in-place "s/.arch i286/.arch i386/" $@

$(TMP)/%.s: $(LIB)/%.cpp $(LIB)/*.h
	@mkdir -p $(TMP)
	$(CXX) $(CXXFLAGS) -I$(LIB) -S $< -o $@
	sed --in-place "s/.arch i286/.arch i386/" $@
